#!/bin/bash
###################################################################### 
#Copyright (C) 2019  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

function main(){
  echo "Welcome..."
  cat <<EOF

    Script by Kris Occhipinti - http://filmsbykris.com

    Based on notes from 
      https://hackaday.com/2018/08/26/arduino-gets-command-line-interface-tools-that-let-you-skip-the-ide/
      https://github.com/arduino/arduino-cli/issues/47

    Creating .cli-config.yml
board_manager:
  additional_urls:
    - http://arduino.esp8266.com/stable/package_esp8266com_index.json
    - https://dl.espressif.com/dl/package_esp32_index.json

EOF

cat << YML > .cli-config.yml
board_manager:
  additional_urls:
    - http://arduino.esp8266.com/stable/package_esp8266com_index.json
    - https://dl.espressif.com/dl/package_esp32_index.json

YML

./arduino-cli core update-index
./arduino-cli core install esp8266:esp8266


  echo "Run this command to compile and upload basic blink code"
  echo "./arduino-cli compile --fqbn esp8266:esp8266:nodemcu basic"
  echo "./arduino-cli upload -p /dev/ttyUSB0 --fqbn esp8266:esp8266:nodemcu basic"
  sleep 1
  exit 0
}

main

